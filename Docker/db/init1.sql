CREATE SCHEMA pirates;

CREATE EXTENSION "uuid-ossp" SCHEMA pirates;

CREATE TABLE pirates.weather (
	id uuid NOT NULL DEFAULT pirates.uuid_generate_v4(),
	temperature float8 NOT NULL,
	city varchar(255) NOT NULL,
	datetime timestamp(0) NOT NULL,
	CONSTRAINT weather_city_pk PRIMARY KEY (id)
);
