(ns woog1e.routes.weather
  (:require
   [woog1e.service.weather :as weather]
   [woog1e.domain.weather.core :as weather-core]
   [woog1e.domain.weather.spec :as ws]
   [clojure.spec.alpha :as s]
   [woog1e.routes.malli.response :as mresponse]))

(def weather-city
  ["/weather/:city"
   {:swagger {:tags [{:name "Weather"}]}
    :get {:summary "Get temperature in given city"
          :responses {200 {:body mresponse/weather}
                      400 {:body mresponse/request-error}
                      404 {:body mresponse/default-error}}
          :parameters {:path [:map [:city [:string {:min 2 :max 255}]]]}
          :handler (fn [{{{:keys [city]} :path} :parameters}]
                     (let [weather (weather/get-weather-in-city city)]
                       (if (s/valid? ::weather/get-weather-in-city-success weather)
                         {:status 200 :body (weather-core/persist-weather! weather)}
                         (if (s/valid? ::weather/get-weather-in-city-failure weather)
                             {:status 404 :body weather}
                             (throw (ex-info "Invalid data returned from function" {:parameters city
                                                                                    :data weather}))))))}}])

