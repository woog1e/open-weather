(ns woog1e.routes.malli.response
  (:require
   [woog1e.core.malli :as mcore]))

(def request-error
  [:map
   [:message [:string]]
   [:uri [:string]]
   [:errors [:map-of :string [:vector string?]]]])

(def default-error
  [:map
   [:error [:string {:json-schema/example "Invalid request"}]]])

(def weather
  [:map
   mcore/city
   mcore/temperature
   mcore/datetime])
