(ns woog1e.service.open-weather
  (:require
   [woog1e.core.config :as config]
   [clj-http.client :as http]
   [clojure.spec.alpha :as s]))

(s/def ::city (s/and string? :woog1e.core.spec/non-blank-string))
(s/def ::state-code (s/and string? :woog1e.core.spec/non-blank-string))
(s/def ::country-code (s/and string? :woog1e.core.spec/non-blank-string))

(s/def ::units #{"standard" "metric" "imperial"})
(s/def ::lang #{"pl" "de" "en"})
(s/def ::mode #{"xml" "html"})

(s/def ::q (s/and string? :woog1e.core.spec/non-blank-string))

(s/def ::city-query (s/keys :req-un [::city ] :opt-un [::state-code ::country-code]))

(s/def ::city-optional (s/keys :opt-un [::units ::lang ::mode]))

(s/def ::method #{http/get http/post http/put http/delete})
(s/def ::opts map?)

(s/fdef call-api
  :args (s/cat :method ::method :opts ::opts))

(s/fdef query-by-city
  :args (s/cat :query ::city-query :opts ::city-optional)
  :ret (s/keys :req-un [::q] :opt-un [::unit ::mode ::lang ])
  :fn #(> (count (:q (:ret %))) 0))

(s/fdef city-get-data
  :args (s/cat :query ::city-query :opts ::city-optional))

(defn call-api
  "Calls passed clj-http method over given opts,
  adds app key to query param and disable exceptions on invalid response status"
  [method opts]
  (method
   (config/value [:open-weather :api-url])
   (-> opts
       (assoc-in [:query-params :appid] (config/value [:open-weather :appid]))
       (assoc-in [:throw-exceptions] false))))

(defn query-by-city
  "Prepare query parameters for city-get-data
  example
  in: {:city \"Berlin\" :country-code \"de\"} {:units \"metric\"}
  out: {:q \"Berlin,de\" :units \"metric\"}
  "
  [query opts]
  (-> {:q (->>  (select-keys query [:city :state-code :country-code])
            (vals)
            (interpose ",")
            (apply str))}
      (merge opts)
      (select-keys [:mode :units :lang :q])))

(defn city-get-data
  "Give back weather in region based on specific city"
  [query opts]
  (call-api http/get {:query-params (query-by-city query opts)}))
