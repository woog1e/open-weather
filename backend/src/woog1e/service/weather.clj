(ns woog1e.service.weather
  (:require
   [woog1e.service.open-weather :as open-weather]
   [woog1e.domain.weather.spec :as ws]
   [clojure.spec.alpha :as s]
   [jsonista.core :as json]))

(s/def ::get-weather-in-city-success (s/keys :req-un [::ws/city ::ws/temperature ::ws/datetime]))
(s/def ::get-weather-in-city-failure (s/keys :req-un [:woog1e.core.spec/error]))

(s/fdef get-weather-in-city
  :args ::ws/city
  :ret (s/or
        :success (s/keys :req-un [::ws/city ::ws/temperature ::ws/datetime])
        :failure :woog1e.core.spec/error-response))

(defn get-weather-in-city
  "Gets weather in given city with current datetime"
  [city]
  (let [datetime (java.time.Instant/now)
        data (open-weather/city-get-data {:city city} {:units "metric"})
        body (-> (:body data) (json/read-value json/keyword-keys-object-mapper))]
      (case (:status data)
        200 {:city city :temperature (:temp (:main body)) :datetime datetime}
        404 {:error (:message body)}
        {:error "Invalid request"})))
