(ns woog1e.domain.weather.store
  (:require
   [woog1e.core.store :as store]
   [woog1e.domain.weather.spec :as ws]
   [next.jdbc :as jdbc]
   [next.jdbc.date-time] ; add support to use java.time.Instant in sql queries, https://cljdoc.org/d/seancorfield/next.jdbc/1.2.659/api/next.jdbc.date-time
   [honey.sql :as sql]
   [honey.sql.helpers :as sql-h]
   [clojure.spec.alpha :as s]))

(next.jdbc.date-time/read-as-instant) ;set default read date from Timtestamp to Instant

(def column :weather)

(def default-options {:qualifier-fn (constantly "woog1e.domain.weather.spec")
                      :label-fn identity})

(s/fdef persist-one!
  :args (s/keys :req [::ws/city ::ws/temperature ::ws/datetime])
  :ret (s/keys :req [::ws/city ::ws/temperature ::ws/datetime ::ws/id]))

(defn persist-one!
  "Persist data as weather in storage"
  [data]
  (store/insert! column data default-options))
