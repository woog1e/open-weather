(ns woog1e.domain.weather.spec
  (:require
   [clojure.spec.alpha :as s]))

(s/def ::id uuid?)
(s/def ::city (s/and string? #(and (<= (count %) 255) (>= (count %) 2))))
(s/def ::datetime (s/and inst? #(instance? java.time.Instant %)))
(s/def ::temperature number?)
