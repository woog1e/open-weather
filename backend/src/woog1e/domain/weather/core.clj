(ns woog1e.domain.weather.core
  (:require
   [woog1e.domain.weather.store :as store]
   [woog1e.domain.weather.spec :as ws]
   [clojure.spec.alpha :as s]))

(s/fdef persist-weather!
  :args (s/keys :req-un [::ws/city ::ws/temperature ::ws/datetime]))

(defn persist-weather!
  "Call store unit to persist weather data in storage"
  [data]
  (store/persist-one! data))
