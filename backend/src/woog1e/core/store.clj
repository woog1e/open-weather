(ns woog1e.core.store
  (:require
   [next.jdbc :as jdbc]
   [next.jdbc.sql :as jdbc-sql]
   [woog1e.core.config :as config]
   [honey.sql :as sql]
   [honey.sql.helpers :as sql-h]
   [next.jdbc.result-set :as rs]))

(def^:private db
  {:dbtype        (config/value [:db :dbtype])
   :port          (config/value [:db :port])
   :user          (config/value [:db :user])
   :password      (config/value [:db :password])
   :dbname        (config/value [:db :dbname])
   :currentSchema (config/value [:db :schema])})

(def^:private ds (jdbc/get-datasource db))
(def^:private ds-opts (jdbc/with-options ds {:builder-fn rs/as-modified-maps}))

(defn execute!
  ([method sql]
  (method ds-opts sql))
  ([method sql opts]
  (method ds-opts sql opts)))

(defn insert!
  ([table key-map]
   (jdbc-sql/insert! ds-opts table key-map))
  ([table key-map opts]
   (jdbc-sql/insert! ds-opts table key-map opts)))
