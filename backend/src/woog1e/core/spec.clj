(ns woog1e.core.spec
  (:require
   [clojure.spec.alpha :as s]
   [clojure.string :as str]))

;; spec for not domain specific values

(s/def ::non-blank-string (complement str/blank?))
(s/def ::error string?)
(s/def ::error-response (s/keys :req-un [::error]))
