(ns woog1e.core.malli
  (:require [woog1e.domain.weather.spec :as ws]))

; in normal app i would use this file for non domain specific values
; but because it is just demo i put here some definitions that are used
; in woog1e.routes.malli/response.clj just for sake of example

(def city
  [::ws/city [:string {:min 2
                   :max 255
                   :json-schema/example "Berlin"}]])

(def temperature
  [::ws/temperature [:double
                 {:json-schema/example 23.3}]])

(def datetime
  [::ws/datetime inst?])
