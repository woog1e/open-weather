(ns woog1e.core.config)

;default clojure way to read edn file
(defn load-config []
     (clojure.edn/read
      (java.io.PushbackReader.
       (clojure.java.io/reader
        (clojure.java.io/resource "env.edn")))))

(defonce config (load-config))

(defn value [key]
  (let [search (if (keyword? key) [key]
                   (if (vector? key) key []))]
    (get-in config search)))

; using areo library for managing configs https://github.com/juxt/aero

   ; minimal example
#_(defn load-config []
    (aero.core/read-config
     (clojure.java.io/resource "env.edn")))
