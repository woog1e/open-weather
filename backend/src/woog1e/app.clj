(ns woog1e.app
  (:gen-class)
  (:require [clojure.tools.logging :as log]
            [reitit.ring :as ring]
            [reitit.http :as http]
            [reitit.coercion.spec]
            [reitit.ring.malli]
            [reitit.swagger :as swagger]
            [reitit.ring.coercion :as rrc]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.exception :as ring-exception]
            [reitit.coercion.malli]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.cors :refer [wrap-cors]]
            [muuntaja.core :as m]
            [woog1e.routes.weather :as weather]))

;; changes keywords to unqualified in data that is passed as json response
(def muuntaja-json-formater
  (muuntaja.format.core/map->Format
   {:name "application/json"
    :decoder [muuntaja.format.json/decoder {:decode-key-fn true}]
    :encoder [muuntaja.format.json/encoder {:encode-key-fn (comp name)}]}))

(def muuntaja (m/create
               (assoc-in muuntaja.core/default-options
                         [:formats "application/json"]
                         muuntaja-json-formater)))

(def cors-middleware
  [#(wrap-cors % :access-control-allow-origin [#".*"] :access-control-allow-methods [:get])])

(defn default-request-log [exception request]
  (log/error exception (format
                        "Exception on %s\nmethod: %s \nbody: %s\npath-params: %s"
                        (:uri request)
                        (:request-method request)
                        (:body-params request)
                        (:path-params request))))

(defn internal-error-response
  [request]
  {:status 500
   :body {:message "Server problem, please try again later"
          :uri (:uri request)
          :data "Internal Error"}})

(defn sql-exception-handler [message exception request]
  (default-request-log exception request)
  (internal-error-response request))

(defn default-exception-handler [message exception request]
  (default-request-log exception request)
  (internal-error-response request))

(defn humanize-malli-errors [message exception request]
  {:status 400
   :body {:message "Invalid request data"
          :errors (malli.error/humanize (ex-data exception))
          :uri (:uri request)}})

(def exception-middleware
  (ring-exception/create-exception-middleware
   (merge
    ring-exception/default-handlers
    {java.sql.SQLException (partial sql-exception-handler "sql-error")
     Exception (partial default-exception-handler "exception-error")
     :reitit.coercion/request-coercion (partial humanize-malli-errors "request-data-error")})))

(def api-routes
  ["/api"
   {:coercion reitit.coercion.malli/coercion}
   ["/swagger.json"
    {:get {:no-doc true
           :swagger {:info {:title "Internal API" :description ""}}
           :handler (swagger/create-swagger-handler)}}]
   weather/weather-city])

(def app
  (ring/ring-handler
   (ring/router
    [api-routes]
    {:data {:muuntaja muuntaja
            :middleware [cors-middleware ;enable CORS for swagger
                         muuntaja/format-middleware
                         exception-middleware
                         rrc/coerce-request-middleware
                         rrc/coerce-response-middleware]}})
   (ring/routes
    (ring/create-default-handler))))

(defn start []
  (jetty/run-jetty #'app {:port 3000, :join? false, :async true}))

(defn -main []
  (start))
