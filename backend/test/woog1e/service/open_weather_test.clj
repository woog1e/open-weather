(ns woog1e.service.open-weather-test
  (:require [woog1e.service.open-weather :as sut]
            [clojure.test :as t]
            [clojure.spec.test.alpha :as stest]
            [clojure.spec.gen.alpha :as gen]
            [clojure.spec.alpha :as s]
            [clojure.string :as str]
            [clj-http.client :as http]))

(t/deftest test-query-by-city-q-commas []
  (let [query (sut/query-by-city
               {:city "Berlin" :state-code "ab" :country-code "de"}
               {:units "metric" :mode "abc" :lang "de"}) ]
    (t/testing "Format of query parameters"
      (t/is (= "abc" (:mode query)))
      (t/is (= "de" (:lang query)))
      (t/is (= "metric" (:units query)))
      (t/is (= "Berlin,ab,de" (:q query))))))

; i'm know we can generate mock data of spec, and then run test of that, i'm using that
; feature during development, this file is just example of unit testsing
